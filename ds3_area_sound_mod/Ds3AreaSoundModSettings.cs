﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Celeste.Mod;

namespace ds3_area_sound_mod
{
	public class Ds3AreaSoundModSettings : EverestModuleSettings
	{
		public bool PlayBeginSound { get; set; } = true;
		public bool PlayEndSound { get; set; } = true;
		public bool PlayLevelDeathSound { get; set; } = true;
		[SettingRange(0, 100)]
		public int SoundVolume { get; set; } = 50;
		// -----
		public bool ShowLevelBeginText { get; set; } = true;
		public bool ShowLevelDeathText { get; set; } = true;
		[SettingInGame(false)]
		public string BeginText { get; set; } = "<name> (<side>)";
		[SettingInGame(false)]
		public string DeathText { get; set; } = "You Died";
	}
}
