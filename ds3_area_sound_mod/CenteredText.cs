﻿using System;
using System.Threading.Tasks;
using Celeste;
using Microsoft.Xna.Framework;
using Monocle;

namespace ds3_area_sound_mod
{
	public class CenteredText : Entity
	{
		public string Text;
		public float Alpha = 1f;
		public uint FadeAfterMs = 2000;
		public Color TextColor = Color.White;
		protected Camera Camera;
		public Level LevelOverride = null;
		private Task FadeTask;

		public CenteredText(string text) : base(Vector2.Zero)
		{
			Text = text;

			Tag = TagsExt.SubHUD | Tags.Persistent | Tags.Global;

			Utils.Log("CenteredText.Create", $"Setting text: {text}");
		}

		public CenteredText(string text, uint fadeAfter) : base(Vector2.Zero)
		{
			Text = text;

			Tag = TagsExt.SubHUD | Tags.Persistent | Tags.Global;

			FadeAfterMs = fadeAfter;

			Utils.Log("CenteredText.Create", $"Setting text: {text}"); 
		}

		public void StartFade(uint duration)
		{
			FadeAfterMs = duration;
			StartFade();
		}

		public void StartFade()
		{
			FadeTask = new Task(async () =>
			{
				var msSleep = FadeAfterMs / 100;
				while (Alpha > 0f)
				{
					await Task.Delay((int)msSleep);
					Alpha -= 0.01f;
				}
			});
			FadeTask.Start();
		}

		public override void Render()
		{
			base.Render();

			if (Alpha <= 0f || string.IsNullOrWhiteSpace(Text))
			{
				return;
			}

			var level = LevelOverride ?? SceneAs<Level>();
			if (level == null)
			{
				return;
			}

			if (Camera == null)
			{
				Camera = level.Camera;
			}

			if (Camera == null)
			{
				return;
			}

			var size = ActiveFont.Measure(Text);

			// TODO: Factor in Text width / size

			var posX = (1920 - Camera.Viewport.Width * 0.5f) * 0.5f + Camera.Viewport.X; //- size.X * 0.5f;
			var posY = (1080 - Camera.Viewport.Height * 0.5f) * 0.5f + Camera.Viewport.Y; //- size.Y * 0.5f;

			var pos = new Vector2(posX, posY);

			const float scaleBy = 2f;

			pos = pos.Clamp(
				0f + size.X * scaleBy, 0f + size.Y * (scaleBy + 0.5f),
				1920f - size.X * scaleBy, 1080f
			);
			

			ActiveFont.DrawOutline(
				Text,
				pos, 
				new Vector2(0.5f, 1f),
				Vector2.One * scaleBy,
				TextColor * Alpha,
				2f,
				Color.Black * (Alpha * Alpha * Alpha)
			);
		}
	}
}