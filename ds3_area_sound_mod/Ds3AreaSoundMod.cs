﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using Celeste;
using Celeste.Mod;
using FMOD.Studio;
using PlayerDeadBody = Celeste.PlayerDeadBody;
using Microsoft.Xna.Framework;
using Level = On.Celeste.Level;
using Player = On.Celeste.Player;

namespace ds3_area_sound_mod
{
	public class Ds3AreaSoundMod : EverestModule
	{
		public static Ds3AreaSoundMod Instance;

		private CenteredText _currentText;

		private EventInstance _lastPlayedSound = null;

		private readonly Regex _levelNameRegex = new Regex("-(\\w+)");

		public Ds3AreaSoundMod() : base()
		{
			Instance = this;
		}

		public override Type SettingsType => typeof(Ds3AreaSoundModSettings);
		public static Ds3AreaSoundModSettings Settings => (Ds3AreaSoundModSettings) Instance._Settings;

		public override Type SaveDataType => null;

		public override void Load()
		{
			Level.Begin += LevelOnBegin;
			Level.End += LevelOnEnd;
			Player.Die += PlayerOnDie;
			Level.TransitionTo += (orig, self, next, direction) =>
			{
				Utils.Log("Transitioned", $"T: {self} {next} {direction}");
				orig(self, next, direction);
			};
		}

		public override void Unload()
		{
			Level.Begin -= LevelOnBegin;
			Level.End -= LevelOnEnd;
			Player.Die -= PlayerOnDie;
		}

		private void LevelOnBegin(Level.orig_Begin orig, Celeste.Level self)
		{
			// Actually exec OnBegin
			orig(self);

			var mapData = self.Session.MapData;

			// Play sound if enabled
			if (Settings.PlayBeginSound)
			{
				Utils.Log("LevelOnBegin#Audio", "Playing Begin sound");
				PlayWithVolume("event:/ds3samod_sfx/level_enter");
			}

			// Display text if enabled
			if (!Settings.ShowLevelBeginText) return;

			Utils.Log("LevelOnBegin#Text", "Created Text");

			var fileNameMatch = _levelNameRegex.Match(mapData.Filename);

			Utils.FLog($"FName: {mapData.Filename}");
			var sessLvl = "Unknown";
			if (fileNameMatch.Success)
			{
				sessLvl = fileNameMatch.Groups[1].Value;
			}
			var sideName = SideName(mapData.Area.Mode);

			_currentText =
				new CenteredText(Settings.BeginText.Replace("<name>", sessLvl).Replace("<side>", sideName), 5000);
			self.Add(_currentText);
			Utils.FLog($"Text After: {_currentText}");
			_currentText.StartFade();
		}

		private void LevelOnEnd(Level.orig_End orig, Celeste.Level self)
		{
			orig(self);

			if (!Settings.PlayEndSound) return;

			Utils.Log("LevelOnEnd", "Playing End sound");
			PlayWithVolume("event:/ds3samod_sfx/level_end");
		}

		private PlayerDeadBody PlayerOnDie(Player.orig_Die orig, Celeste.Player self, Vector2 direction,
			bool evenifinvincible, bool registerdeathinstats)
		{
			var pdead = orig(self, direction, evenifinvincible, registerdeathinstats);

			if (Settings.PlayLevelDeathSound)
			{
				Utils.Log("PlayerOnDie", "Playing Death sound");
				PlayWithVolume("event:/ds3samod_sfx/death");
			}

			_currentText = new CenteredText(Settings.DeathText)
			{
				TextColor = Color.IndianRed,
				FadeAfterMs = 2000
			};
			_currentText.StartFade();

			return pdead;
		}

		private EventInstance PlayWithVolume(string eventPath)
		{
			if (_lastPlayedSound != null)
			{
				// Force stop other sounds spawned by this mod before playing a new one
				// this may create a place where one sound "eats" the other
				// but it fixes the sound hell in assist-invicibilty mode
				_lastPlayedSound.stop(STOP_MODE.IMMEDIATE);
			}

			var evInst = Celeste.Audio.Play(eventPath);
			Utils.Log("playWithVolume", $"Settings VOL: {Settings.SoundVolume}");
			evInst.setVolume(Monocle.Calc.Map(Settings.SoundVolume, 0.0f, 100.0f));
			_lastPlayedSound = evInst;
			return _lastPlayedSound;
		}

		private string SideName(AreaMode mode)
		{
			switch (mode)
			{
				case AreaMode.Normal:
					return "A Side";
				case AreaMode.BSide:
					return "B Side";
				case AreaMode.CSide:
					return "C Side";
				default:
					return "Unknown Side";
			}
		}
	}
}