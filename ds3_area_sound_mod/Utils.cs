﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Celeste.Mod;

namespace ds3_area_sound_mod
{
	class Utils
	{
		public static void Log(string method, string msg, LogLevel lvl = LogLevel.Verbose)
		{
			Logger.Log(lvl, "ds3_area_sound_mod", $"{method}: {msg}");
		}

		public static void FLog(string msg)
		{
			Logger.Log(LogLevel.Verbose, "ds3_area_sound_mod", $"| ^ |: {msg}");
		}
	}
}
